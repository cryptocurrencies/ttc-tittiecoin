# ---- Base Node ----
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ----
FROM base as build
RUN git clone https://github.com/tittiecoin/tittiecoin.git /opt/tittiecoin && \
    cd /opt/tittiecoin/src && \
    make -f makefile.unix

# ---- Release ----
FROM ubuntu:16.04 AS release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r tittiecoin && useradd -r -m -g tittiecoin tittiecoin
RUN mkdir /data
RUN chown tittiecoin:tittiecoin /data
COPY --from=build /opt/tittiecoin/src/TittieCoind /usr/local/bin/
USER tittiecoin
VOLUME /data
EXPOSE 7408 7474
CMD ["/usr/local/bin/TittieCoind", "-datadir=/data", "-conf=/data/TittieCoin.conf", "-server", "-txindex", "-printtoconsole"]